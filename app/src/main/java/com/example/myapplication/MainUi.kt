package com.example.myapplication

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import github.masterj3y.navigation.Screen
import github.masterj3y.navigation.tabs
import github.masterj3y.profile.UserProfileUi
import github.masterj3y.projects.ProjectsListUi
import github.masterj3y.projects.saved.SavedProjectsListUi

@Composable
fun MainUi() {
    val navController = rememberNavController()
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    val isSavedProjectsListOpened by remember(currentDestination) {
        derivedStateOf { currentDestination?.hierarchy?.any { it.route == Screen.SavedProjectsList.route } == true }
    }

    Scaffold(
        topBar = {
            SmallTopAppBar(
                title = { Text(text = "Github") }, colors = TopAppBarDefaults.smallTopAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primary,
                    titleContentColor = MaterialTheme.colorScheme.onPrimary
                ),
                actions = {
                    AnimatedVisibility(
                        visible = !isSavedProjectsListOpened,
                        enter = fadeIn(),
                        exit = fadeOut()
                    ) {
                        SavedProjectsButton(
                            modifier = Modifier
                                .padding(end = 8.dp)
                                .clip(MaterialTheme.shapes.small)
                                .clickable {
                                    if (!isSavedProjectsListOpened)
                                        navController.navigate(Screen.SavedProjectsList.route)
                                }
                                .padding(8.dp),
                            title = Screen.SavedProjectsList.title
                        )
                    }
                }
            )
        },
        bottomBar = {
            NavigationBar {
                tabs.forEach { screen ->
                    NavigationBarItem(
                        icon = { Icon(screen.icon, contentDescription = null) },
                        label = { Text(screen.title) },
                        selected = currentDestination?.hierarchy?.any { it.route == screen.route } == true,
                        onClick = {
                            navController.navigate(screen.route) {
                                popUpTo(navController.graph.findStartDestination().id) {
                                    saveState = true
                                }
                                launchSingleTop = true
                                restoreState = true
                            }
                        }
                    )
                }
            }
        }
    ) { innerPadding ->
        NavHost(
            navController,
            startDestination = Screen.Profile.route,
            Modifier.padding(innerPadding)
        ) {
            composable(Screen.Profile.route) { UserProfileUi() }
            composable(Screen.ProjectsList.route) { ProjectsListUi() }
            composable(Screen.SavedProjectsList.route) { SavedProjectsListUi() }
        }
    }
}

@Composable
private fun SavedProjectsButton(modifier: Modifier = Modifier, title: String) {
    CompositionLocalProvider(LocalContentColor provides MaterialTheme.colorScheme.onPrimary) {
        Row(
            modifier = modifier,
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Icon(
                imageVector = Screen.SavedProjectsList.icon,
                contentDescription = null
            )
            Text(text = title)
        }
    }
}