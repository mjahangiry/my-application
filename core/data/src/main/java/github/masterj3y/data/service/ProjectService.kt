package github.masterj3y.data.service

import com.skydoves.sandwich.ApiResponse
import github.masterj3y.data.model.ProjectUpdate
import github.masterj3y.data.model.StarredProject
import retrofit2.http.GET
import retrofit2.http.Path

interface ProjectService {

    @GET("users/{userName}/starred")
    suspend fun getStarredProjects(
        @Path("userName")
        userName: String
    ): ApiResponse<List<StarredProject>>

    @GET("users/{userName}/repos")
    suspend fun getProjects(
        @Path("userName")
        userName: String
    ): ApiResponse<List<ProjectUpdate>>
}