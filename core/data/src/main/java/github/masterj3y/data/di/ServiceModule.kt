package github.masterj3y.data.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import github.masterj3y.data.service.ProjectService
import github.masterj3y.data.service.UserProfileService
import retrofit2.Retrofit
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Provides
    @Singleton
    fun provideUserService(retrofit: Retrofit): UserProfileService = retrofit.create()

    @Provides
    @Singleton
    fun provideProjectService(retrofit: Retrofit): ProjectService = retrofit.create()
}