package github.masterj3y.data.extension

import arrow.core.Either
import com.skydoves.sandwich.ApiResponse
import com.skydoves.sandwich.getOrNull
import github.masterj3y.data.error.FetchDataError

fun <R> ApiResponse<R>.either(): Either<FetchDataError, R> = getOrNull()
    ?.let { Either.Right(it) }
    ?: Either.Left(FetchDataError())