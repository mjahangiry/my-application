package github.masterj3y.data.model

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class ProjectUpdate(
    @ColumnInfo(name = "id")
    @SerializedName("id")
    val id: Int,
    @ColumnInfo(name = "name")
    @SerializedName("name")
    val name: String,
    @ColumnInfo(name = "full_name")
    @SerializedName("full_name")
    val fullName: String,
    @ColumnInfo(name = "description")
    @SerializedName("description")
    val description: String?,
    @ColumnInfo(name = "stargazers_count")
    @SerializedName("stargazers_count")
    val stargazersCount: Int,
    @ColumnInfo(name = "language")
    @SerializedName("language")
    val language: String?,
)