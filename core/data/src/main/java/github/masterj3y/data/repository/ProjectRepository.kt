package github.masterj3y.data.repository

import arrow.core.Either
import github.masterj3y.data.database.entity.Project
import github.masterj3y.data.model.StarredProject
import kotlinx.coroutines.flow.Flow

interface ProjectRepository {

    val projects: Flow<List<Project>>

    val savedProjects: Flow<List<Project>>

    suspend fun getStarredProjects(userName: String): Either<Error, List<StarredProject>>

    suspend fun refreshProjects(userName: String): Boolean

    suspend fun toggleIsSavedProject(projectId: Int, isSavedAlready: Boolean)
}