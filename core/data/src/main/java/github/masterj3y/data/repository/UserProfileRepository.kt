package github.masterj3y.data.repository

import arrow.core.Either
import github.masterj3y.data.model.UserProfile

interface UserProfileRepository {

    suspend fun getUserProfile(userName: String): Either<Error, UserProfile>
}