package github.masterj3y.data.error

class FetchDataError : Error("Can't fetch data")