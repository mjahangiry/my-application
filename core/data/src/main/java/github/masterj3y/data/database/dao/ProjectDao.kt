package github.masterj3y.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import github.masterj3y.data.database.entity.Project
import github.masterj3y.data.model.ProjectUpdate
import kotlinx.coroutines.flow.Flow

@Dao
interface ProjectDao {

    @Insert(entity = Project::class, onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(vararg project: ProjectUpdate)

    @Query("SELECT * FROM  projects")
    fun findAllProjects(): Flow<List<Project>>

    @Query("SELECT * FROM  projects WHERE is_saved == 1")
    fun findAllSavedProjects(): Flow<List<Project>>

    @Query("UPDATE projects SET is_saved = :isSaved WHERE id = :id")
    suspend fun updateSaved(id: Int, isSaved: Boolean)
}