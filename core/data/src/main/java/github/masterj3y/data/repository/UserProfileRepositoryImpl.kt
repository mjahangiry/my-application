package github.masterj3y.data.repository

import arrow.core.Either
import github.masterj3y.data.extension.either
import github.masterj3y.data.model.UserProfile
import github.masterj3y.data.service.UserProfileService
import javax.inject.Inject

class UserProfileRepositoryImpl
@Inject
constructor(private val service: UserProfileService) : UserProfileRepository {

    override suspend fun getUserProfile(userName: String): Either<Error, UserProfile> =
        service.getUserProfile(userName).either()
}