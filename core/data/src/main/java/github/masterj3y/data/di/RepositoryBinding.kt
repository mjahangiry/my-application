package github.masterj3y.data.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import github.masterj3y.data.repository.ProjectRepository
import github.masterj3y.data.repository.ProjectRepositoryImpl
import github.masterj3y.data.repository.UserProfileRepository
import github.masterj3y.data.repository.UserProfileRepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal interface RepositoryBinding {

    @Binds
    @Singleton
    fun bindUserProfileRepository(userProfileRepositoryImpl: UserProfileRepositoryImpl): UserProfileRepository

    @Binds
    @Singleton
    fun bindProjectRepository(projectRepositoryImpl: ProjectRepositoryImpl): ProjectRepository
}