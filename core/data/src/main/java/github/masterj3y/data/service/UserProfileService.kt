package github.masterj3y.data.service

import com.skydoves.sandwich.ApiResponse
import github.masterj3y.data.model.UserProfile
import retrofit2.http.GET
import retrofit2.http.Path

interface UserProfileService {

    @GET("users/{userName}")
    suspend fun getUserProfile(
        @Path("userName") userName: String
    ): ApiResponse<UserProfile>
}