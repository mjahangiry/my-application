package github.masterj3y.data.repository

import arrow.core.Either
import com.skydoves.sandwich.suspendOnSuccess
import github.masterj3y.data.database.AppDatabase
import github.masterj3y.data.database.entity.Project
import github.masterj3y.data.extension.either
import github.masterj3y.data.model.StarredProject
import github.masterj3y.data.service.ProjectService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ProjectRepositoryImpl
@Inject
constructor(
    private val service: ProjectService,
    private val database: AppDatabase
) : ProjectRepository {

    override val projects: Flow<List<Project>> =
        database.projectDao().findAllProjects()

    override val savedProjects: Flow<List<Project>> =
        database.projectDao().findAllSavedProjects()

    override suspend fun getStarredProjects(userName: String): Either<Error, List<StarredProject>> =
        service.getStarredProjects(userName).either()

    override suspend fun refreshProjects(userName: String): Boolean {
        return service.getProjects(userName).suspendOnSuccess {
            database.projectDao().insert(*data.toTypedArray())
        }.either().isRight()
    }

    override suspend fun toggleIsSavedProject(projectId: Int, isSavedAlready: Boolean) {
        database.projectDao().updateSaved(projectId, !isSavedAlready)
    }
}