package github.masterj3y.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import github.masterj3y.data.database.dao.ProjectDao
import github.masterj3y.data.database.entity.Project

@Database(
    entities = [Project::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun projectDao(): ProjectDao
}