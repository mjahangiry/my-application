package github.masterj3y.data.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Owner(
    @SerializedName("login")
    val login: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("avatar_url")
    val avatarUrl: String,
    @SerializedName("html_url")
    val htmlUrl: String,
)