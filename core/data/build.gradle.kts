plugins {
    id("android.library")
    id("android.hilt")
    id("com.google.devtools.ksp")
}

android {
    namespace = "github.masterj3y.data"
}

dependencies {
    api(project(":core:network"))

    api(libs.arrowkt.core)

    implementation(libs.androidx.room.ktx)
    ksp(libs.androidx.room.compiler)
}