plugins {
    id("android.library")
    id("android.library.compose")
}

android {
    namespace = "github.masterj3y.designsystem"
}

dependencies {

    // material
    api(libs.androidx.appcompat)
    api(libs.androidx.compose.material3)

    api(libs.androidx.compose.runtime)
    api(libs.androidx.compose.ui)
    api(libs.androidx.compose.ui.tooling)
    api(libs.androidx.compose.ui.tooling.preview)
    api(libs.androidx.compose.material.iconsExtended)
    api(libs.androidx.compose.material3)
    api(libs.androidx.compose.foundation)
    api(libs.androidx.compose.foundation.layout)
}