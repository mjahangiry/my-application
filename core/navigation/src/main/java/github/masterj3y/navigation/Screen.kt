package github.masterj3y.navigation

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Article
import androidx.compose.material.icons.filled.Bookmark
import androidx.compose.material.icons.filled.Person
import androidx.compose.ui.graphics.vector.ImageVector

sealed class Screen(val route: String, val title: String, val icon: ImageVector) {
    object Profile : Screen(route = "profile", title = "Profile", icon = Icons.Filled.Person)
    object ProjectsList :
        Screen(route = "projects", title = "Repositories", icon = Icons.Filled.Article)

    object SavedProjectsList :
        Screen(
            route = "saved_projects",
            title = "Saved Projects List",
            icon = Icons.Filled.Bookmark
        )
}

val tabs = listOf(
    Screen.Profile,
    Screen.ProjectsList,
)