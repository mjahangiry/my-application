plugins {
    id("android.library")
}

android {
    namespace = "github.masterj3y.navigation"
}

dependencies {

    implementation(project(":core:designsystem"))

    api(libs.androidx.navigation.compose)
}