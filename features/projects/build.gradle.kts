plugins {
    id("android.library")
    id("android.library.compose")
    id("android.feature")
    id("android.hilt")
}

android {
    namespace = "github.masterj3y.projects"
}

dependencies {

    implementation(libs.androidx.lifecycle.runtimeCompose)
    implementation(libs.androidx.lifecycle.viewModelCompose)

    implementation(libs.glide)
}