package github.masterj3y.projects.saved

import androidx.compose.runtime.Immutable
import github.masterj3y.data.database.entity.Project

@Immutable
class SavedProjectsListUiState(
    val savedProjects: List<Project>
) {

    companion object {
        val initial = SavedProjectsListUiState(savedProjects = emptyList())
    }
}