package github.masterj3y.projects

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import github.masterj3y.data.Consts
import github.masterj3y.data.coroutines.WhileSubscribedOrRetained
import github.masterj3y.data.error.FetchDataError
import github.masterj3y.data.repository.ProjectRepository
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProjectsViewModel
@Inject constructor(private val repository: ProjectRepository) : ViewModel() {

    private val _uiState = MutableStateFlow(ProjectsListUiState.initial)
    val uiState = _uiState.stateIn(viewModelScope, WhileSubscribedOrRetained, _uiState.value)

    init {
        loadProjects()

        repository.projects
            .onEach { projects ->
                _uiState.update { it.copy(projects = projects) }
            }
            .launchIn(viewModelScope)
    }

    fun loadProjects() {
        _uiState.update { it.copy(loading = true, error = null) }
        viewModelScope.launch {
            if (repository.refreshProjects(Consts.USER_NAME))
                _uiState.update {
                    it.copy(
                        loading = false
                    )
                }
            else
                _uiState.update { it.copy(loading = false, error = FetchDataError()) }
        }
    }

    fun toggleIsSavedProject(projectId: Int, isSavedAlready: Boolean) {
        viewModelScope.launch { repository.toggleIsSavedProject(projectId, isSavedAlready) }
    }
}