package github.masterj3y.projects

import androidx.compose.runtime.Immutable
import github.masterj3y.data.database.entity.Project

@Immutable
data class ProjectsListUiState(
    val loading: Boolean,
    val projects: List<Project>,
    val error: Error?
) {

    companion object {
        val initial: ProjectsListUiState
            get() = ProjectsListUiState(
                loading = false,
                projects = emptyList(),
                error = null
            )
    }
}