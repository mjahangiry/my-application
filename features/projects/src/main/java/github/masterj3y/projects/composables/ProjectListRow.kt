package github.masterj3y.projects.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Code
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.outlined.Bookmark
import androidx.compose.material.icons.outlined.BookmarkBorder
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import github.masterj3y.data.database.entity.Project
import github.masterj3y.data.extension.compact

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ProjectsList(list: List<Project>, onItemClick: (Project) -> Unit) {
    LazyColumn(verticalArrangement = Arrangement.spacedBy(16.dp)) {
        items(list, key = { it.id }) { item ->
            ProjectRow(
                modifier = Modifier
                    .animateItemPlacement()
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                item = item,
                onClick = onItemClick
            )
        }
    }
}

@Composable
private fun ProjectRow(modifier: Modifier = Modifier, item: Project, onClick: (Project) -> Unit) {
    Surface(
        modifier = modifier,
        tonalElevation = 1.dp,
        shape = MaterialTheme.shapes.medium
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .clickable { onClick(item) }
                .padding(16.dp),
        ) {
            Text(text = item.fullName, style = MaterialTheme.typography.titleSmall)
            item.description?.let {
                Text(
                    text = it,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis,
                    style = MaterialTheme.typography.labelSmall
                )
            }
            Spacer(modifier = Modifier.height(16.dp))
            Row(verticalAlignment = Alignment.CenterVertically) {
                Icon(
                    imageVector = Icons.Filled.Star,
                    tint = Color.Yellow,
                    contentDescription = null
                )
                Spacer(modifier = Modifier.width(8.dp))
                Text(text = item.stargazersCount.compact())
                item.language?.let {
                    Spacer(modifier = Modifier.width(16.dp))
                    Icon(
                        imageVector = Icons.Filled.Code,
                        contentDescription = null
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                    Text(
                        modifier = Modifier.padding(end = 8.dp),
                        text = it,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                }
                Spacer(modifier = Modifier.weight(1f))
                AnimatedVisibility(visible = !(item.isSaved ?: false)) {
                    Icon(
                        imageVector = Icons.Outlined.BookmarkBorder,
                        contentDescription = null
                    )
                }
                AnimatedVisibility(visible = item.isSaved == true) {
                    Icon(
                        imageVector = Icons.Outlined.Bookmark,
                        contentDescription = null
                    )
                }
            }
        }
    }
}