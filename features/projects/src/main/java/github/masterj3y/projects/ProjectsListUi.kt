package github.masterj3y.projects

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import github.masterj3y.projects.composables.ProjectsList

@Composable
fun ProjectsListUi(viewModel: ProjectsViewModel = hiltViewModel()) {

    val uiState by viewModel.uiState.collectAsState()

    ProjectsList(
        list = uiState.projects,
        onItemClick = { viewModel.toggleIsSavedProject(it.id, it.isSaved == true) }
    )
}