package github.masterj3y.projects.saved

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import github.masterj3y.projects.composables.ProjectsList

@Composable
fun SavedProjectsListUi(viewModel: SavedProjectsListViewModel = hiltViewModel()) {

    val uiState by viewModel.uiState.collectAsState()

    ProjectsList(
        list = uiState.savedProjects,
        onItemClick = { viewModel.toggleIsSavedProject(it.id, it.isSaved == true) })
}