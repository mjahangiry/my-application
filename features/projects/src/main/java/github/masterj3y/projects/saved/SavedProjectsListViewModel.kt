package github.masterj3y.projects.saved

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import github.masterj3y.data.coroutines.WhileSubscribedOrRetained
import github.masterj3y.data.repository.ProjectRepository
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SavedProjectsListViewModel
@Inject constructor(
    private val repository: ProjectRepository
) : ViewModel() {

    val uiState = repository.savedProjects
        .map {
            SavedProjectsListUiState(it)
        }
        .stateIn(viewModelScope, WhileSubscribedOrRetained, SavedProjectsListUiState.initial)

    fun toggleIsSavedProject(projectId: Int, isSavedAlready: Boolean) {
        viewModelScope.launch { repository.toggleIsSavedProject(projectId, isSavedAlready) }
    }
}