package github.masterj3y.profile

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Code
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.outlined.StarOutline
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import github.masterj3y.data.extension.compact
import github.masterj3y.data.model.StarredProject
import github.masterj3y.designsystem.composables.ErrorLayout
import github.masterj3y.designsystem.composables.Loading


@Composable
fun StarredRepositories(
    starredProjects: List<StarredProject>,
    loading: Boolean,
    error: Error?,
    onRetryClick: () -> Unit
) {
    Surface(modifier = Modifier.animateContentSize()) {

        Column(
            Modifier
                .fillMaxWidth()
        ) {
            Row(
                modifier = Modifier.padding(16.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(imageVector = Icons.Outlined.StarOutline, contentDescription = null)
                Spacer(modifier = Modifier.width(8.dp))
                Text(
                    text = "Starred Repositories",
                    style = MaterialTheme.typography.titleLarge
                )
            }

            if (loading) {
                Loading(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 32.dp)
                )
                return@Column
            }

            if (error != null) {
                ErrorLayout(onRetryClick = onRetryClick)
                return@Column
            }

            LazyRow(
                horizontalArrangement = Arrangement.spacedBy(8.dp),
            ) {
                item { Spacer(modifier = Modifier.width(8.dp)) }
                items(starredProjects) {
                    StarredProjectRow(item = it)
                }
                item { Spacer(modifier = Modifier.width(8.dp)) }
            }
        }
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun StarredProjectRow(item: StarredProject) {
    Surface(
        modifier = Modifier.size(width = 320.dp, height = 165.dp),
        tonalElevation = 5.dp,
        shape = MaterialTheme.shapes.medium
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                GlideImage(
                    modifier = Modifier
                        .size(25.dp)
                        .clip(CircleShape),
                    model = item.owner.avatarUrl,
                    contentDescription = null
                )
                CompositionLocalProvider(
                    LocalContentColor provides LocalContentColor.current.copy(alpha = .8f)
                ) {
                    Text(text = item.owner.login, style = MaterialTheme.typography.labelMedium)
                }
            }
            Spacer(modifier = Modifier.height(8.dp))
            Text(text = item.fullName, style = MaterialTheme.typography.titleSmall)
            item.description?.let {
                Text(
                    text = it,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis,
                    style = MaterialTheme.typography.labelSmall
                )
            }
            Spacer(modifier = Modifier.weight(1f))
            Row(verticalAlignment = Alignment.CenterVertically) {
                Icon(
                    imageVector = Icons.Filled.Star,
                    tint = Color.Yellow,
                    contentDescription = null
                )
                Spacer(modifier = Modifier.width(8.dp))
                Text(text = item.stargazersCount.compact())
                item.language?.let {
                    Spacer(modifier = Modifier.width(16.dp))
                    Icon(
                        imageVector = Icons.Filled.Code,
                        contentDescription = null
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                    Text(text = it)
                }
            }
        }
    }
}