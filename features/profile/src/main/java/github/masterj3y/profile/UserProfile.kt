package github.masterj3y.profile

import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import github.masterj3y.data.extension.compact
import github.masterj3y.data.model.UserProfile
import github.masterj3y.designsystem.composables.ErrorLayout
import github.masterj3y.designsystem.composables.Loading

@Composable
fun UserProfile(
    userProfile: UserProfile?,
    loading: Boolean,
    error: Error?,
    onRetryClick: () -> Unit
) {

    val context = LocalContext.current

    Surface(modifier = Modifier.animateContentSize()) {

        if (error != null) {
            ErrorLayout(onRetryClick = onRetryClick)
            return@Surface
        }

        if (loading)
            Loading(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 32.dp)
            )

        if (userProfile == null)
            return@Surface

        Column(
            Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Header(userProfile = userProfile)
            Spacer(modifier = Modifier.height(16.dp))
            userProfile.bio?.let {
                Text(text = it)
                Spacer(modifier = Modifier.height(8.dp))
            }
            ContactInfo(userProfile = userProfile)
            Spacer(modifier = Modifier.height(16.dp))
            Connections(userProfile = userProfile)
            Spacer(modifier = Modifier.height(32.dp))
            OpenInBrowserButton(modifier = Modifier.fillMaxWidth()) {
                CustomTabsIntent.Builder()
                    .build()
                    .launchUrl(context, Uri.parse(userProfile.htmlUrl))
            }
        }
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun Header(userProfile: UserProfile) {
    Row(
        modifier = Modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        GlideImage(
            modifier = Modifier
                .size(70.dp)
                .clip(CircleShape),
            model = userProfile.avatarUrl,
            contentDescription = null
        )
        Spacer(modifier = Modifier.width(16.dp))
        Column {
            Text(
                text = userProfile.name,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography.headlineMedium.copy(fontWeight = FontWeight.Bold)
            )
            Text(
                text = userProfile.login,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography.bodyMedium
            )
        }
    }
}

@Composable
private fun ContactInfo(userProfile: UserProfile) {
    Column(verticalArrangement = Arrangement.spacedBy(8.dp)) {
        with(userProfile) {
            company?.let { ContactInfoRow(icon = Icons.Outlined.Business, title = it) }
            location?.let { ContactInfoRow(icon = Icons.Outlined.LocationOn, title = it) }
            if (!blog.isNullOrBlank()) {
                ContactInfoRow(icon = Icons.Outlined.Link, title = blog!!)
            }
            twitterUsername?.let {
                ContactInfoRow(
                    icon = Icons.Outlined.Contacts,
                    title = it
                )
            }
        }
    }
}

@Composable
private fun ContactInfoRow(icon: ImageVector, title: String) {
    ContactInfoRow(icon = icon) {
        CompositionLocalProvider(LocalContentColor provides LocalContentColor.current.copy(alpha = .8f)) {
            Text(
                text = title,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography.bodyMedium
            )
        }
    }
}

@Composable
private fun ContactInfoRow(icon: ImageVector, content: @Composable RowScope.() -> Unit) {
    Row(
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        CompositionLocalProvider(LocalContentColor provides LocalContentColor.current.copy(alpha = .5f)) {
            Icon(imageVector = icon, contentDescription = null)
        }
        content()
    }
}

@Composable
private fun Connections(userProfile: UserProfile) {
    ContactInfoRow(icon = Icons.Outlined.Person) {
        Text(
            text = userProfile.followers.compact(),
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
        )

        Text(
            text = "followers",
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.bodyMedium
        )

        Text(
            text = "  " + userProfile.following.compact(),
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
        )

        Text(
            text = "following",
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.bodyMedium
        )
    }
}

@Composable
private fun OpenInBrowserButton(modifier: Modifier = Modifier, onClick: () -> Unit) {
    OutlinedButton(modifier = modifier, onClick = onClick) {
        Icon(imageVector = Icons.Outlined.Link, contentDescription = null)
        Spacer(modifier = Modifier.width(8.dp))
        Text(text = "Open profile in browser")
    }
}