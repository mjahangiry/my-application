package github.masterj3y.profile

import androidx.compose.runtime.Immutable
import github.masterj3y.data.model.StarredProject
import github.masterj3y.data.model.UserProfile

@Immutable
data class UserProfileUiState(
    val loadingUserProfile: Boolean,
    val userProfile: UserProfile?,
    val loadingStarredProjects: Boolean,
    val starredProjects: List<StarredProject>,
    val userProfileError: Error?,
    val starredProjectsError: Error?
) {

    companion object {
        val initial: UserProfileUiState
            get() = UserProfileUiState(
                loadingUserProfile = false,
                userProfile = null,
                loadingStarredProjects = false,
                starredProjects = emptyList(),
                userProfileError = null,
                starredProjectsError = null
            )
    }
}