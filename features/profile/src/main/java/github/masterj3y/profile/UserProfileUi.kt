package github.masterj3y.profile

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Divider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel

@Composable
fun UserProfileUi(viewModel: UserProfileViewModel = hiltViewModel()) {

    val uiState by viewModel.uiState.collectAsState()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
    ) {
        UserProfile(
            userProfile = uiState.userProfile,
            loading = uiState.loadingUserProfile,
            error = uiState.userProfileError,
            onRetryClick = viewModel::loadUserProfile
        )
        Divider(
            Modifier
                .fillMaxWidth()
                .background(Color.Gray.copy(alpha = .25f))
                .height(8.dp)
                .padding(vertical = 12.dp)
        )
        StarredRepositories(
            starredProjects = uiState.starredProjects,
            loading = uiState.loadingStarredProjects,
            error = uiState.starredProjectsError,
            onRetryClick = viewModel::loadStarredProjects
        )
    }
}
