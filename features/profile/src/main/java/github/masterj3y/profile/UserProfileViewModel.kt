package github.masterj3y.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import arrow.core.Either
import dagger.hilt.android.lifecycle.HiltViewModel
import github.masterj3y.data.Consts
import github.masterj3y.data.coroutines.WhileSubscribedOrRetained
import github.masterj3y.data.repository.ProjectRepository
import github.masterj3y.data.repository.UserProfileRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserProfileViewModel
@Inject
constructor(
    private val userProfileRepository: UserProfileRepository,
    private val projectRepository: ProjectRepository
) : ViewModel() {

    private val _uiState = MutableStateFlow(UserProfileUiState.initial)
    val uiState = _uiState.stateIn(viewModelScope, WhileSubscribedOrRetained, _uiState.value)

    init {
        loadUserProfile()
        loadStarredProjects()
    }

    fun loadUserProfile() {
        _uiState.update { it.copy(loadingUserProfile = true, userProfileError = null) }

        viewModelScope.launch {
            when (val data = userProfileRepository.getUserProfile(Consts.USER_NAME)) {
                is Either.Right -> _uiState.update {
                    it.copy(
                        loadingUserProfile = false,
                        userProfile = data.value
                    )
                }
                is Either.Left -> _uiState.update {
                    it.copy(
                        loadingUserProfile = false,
                        userProfileError = data.value
                    )
                }
            }
        }
    }

    fun loadStarredProjects() {
        _uiState.update { it.copy(loadingStarredProjects = true, starredProjectsError = null) }

        viewModelScope.launch {
            when (val data = projectRepository.getStarredProjects(Consts.USER_NAME)) {
                is Either.Right -> _uiState.update {
                    it.copy(
                        loadingStarredProjects = false,
                        starredProjects = data.value
                    )
                }
                is Either.Left -> _uiState.update {
                    it.copy(
                        loadingStarredProjects = false,
                        starredProjectsError = data.value
                    )
                }
            }
        }
    }
}